#!/usr/bin/env python3
from dotenv import load_dotenv
from mastodon import Mastodon
import twitter
import os
import json
import time

load_dotenv()

TWITTER_CONSUMER_KEY = os.getenv('TWITTER_CONSUMER_KEY')
TWITTER_CONSUMER_SECRET = os.getenv('TWITTER_CONSUMER_SECRET')
TWITTER_ACCESS_TOKEN = os.getenv('TWITTER_ACCESS_TOKEN')
TWITTER_ACCESS_TOKEN_SECRET = os.getenv('TWITTER_ACCESS_TOKEN_SECRET')
MASTODON_ACCESS_TOKEN = os.getenv('MASTODON_ACCESS_TOKEN')
MASTODON_URL = os.getenv('MASTODON_URL')
CACHE_FILE = '.tweet_cache'

CACHED_TWEETS = []      # tweet_ids from the cache file
tweets_tooted = []      # tweet_ids currently being reposted
time_start = 0          # timer used for TwitterAPI rate limit


def loadCache(filename):
    with open(filename) as file:
        for line in file:
            #print(line.strip())
            CACHED_TWEETS.append(line.strip())
    file.close()

def getHomeTimeline(c):
    api = twitter.Api(TWITTER_CONSUMER_KEY,
                    TWITTER_CONSUMER_SECRET,
                    TWITTER_ACCESS_TOKEN,
                    TWITTER_ACCESS_TOKEN_SECRET)
    try:
        statuses = api.GetHomeTimeline(count=c, exclude_replies=True, contributor_details=True)
        global time_start
        time_start = time.time()
        print("time_start updated: %s" %time_start)
        return statuses
    except:
        raise

def postMastodonStatus(t_id, screen_name, name, text):
    toot_text = '{0} ::: {1} ::: https://twitter.com/{2}/status/{3}'.format(name, text, screen_name, t_id)
    print(toot_text)
    M = Mastodon(
        access_token = MASTODON_ACCESS_TOKEN,
        api_base_url = MASTODON_URL
    )
    response = M.toot(toot_text)
    if 'id' in response:
        print('success')
        tweets_tooted.append(t_id)
    else:
        raise


def main():
    loadCache(CACHE_FILE)
    elapsed_time = time.time() - time_start
    print("time_start: %s" %time_start)
    print("elapsed_time: %s" %elapsed_time)
    if time_start != 0:
        if elapsed_time < 60:
            print("time to sleep")
            time.sleep(60-elapsed_time)
    timeline = getHomeTimeline(200)

    if (timeline != None):
        skip_count = 0
        for tweet in timeline:
            #print(tweet.id_str)
            if tweet.id_str in tweets_tooted or tweet.id_str in CACHED_TWEETS:
                skip_count = skip_count + 1
                #print("already tooted")
            else:
                time.sleep(1)
                postMastodonStatus(
                    tweet.id_str,
                    tweet.user.screen_name,
                    tweet.user.name,
                    tweet.text
                )
        print("%s tweets had already been forwarded" %skip_count)
    tweet_cache = open(CACHE_FILE, 'a')
    for item in tweets_tooted:
        tweet_cache.write("%s\n" % item)
    tweet_cache.close()
    tweets_tooted[:] = []
    print("Done.")

if __name__ == "__main__":
    while True:
        main()
        print("main loop sleep")
        time.sleep(3)
