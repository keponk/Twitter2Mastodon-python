# Twitter timeline to Mastodon (bot)

Bot that reposts your twitter timeline to mastodon.

## Usage
Best to use a virtual environment. 

1. Create venv 
```
virtualenv venv --python=python3
source venv/bin/activate
```

2. Install requirements
```
pip install -r requirements.txt
```

3. Create `.env` file with the twitter and mastodon crentials/tokens. Something like:
```
MASTODON_URL=https://botsin.space  # or whatever instance your bot will be running on
MASTODON_ACCESS_TOKEN=XXX
TWITTER_SCREEN_NAME=your_twitter_handle
TWITTER_CONSUMER_KEY=XXX
TWITTER_CONSUMER_SECRET=XXX
TWITTER_ACCESS_TOKEN=XXX-XXX
TWITTER_ACCESS_TOKEN_SECRET=XXX
``` 
Note: You can get your mastodon access token from many places, I used https://tinysubversions.com/notes/mastodon-bot/


4. Run
```
python main.py
```

Toots will be in the form `Long_twitter_name ::: tweet content ::: link to tweet`, no particular reason, I just found this pleasant enough but it's easy to change.


## Improvements
There's many things that would be cool to do like 
* copying media along with each toot
* have a way so that the bot is able to reply (although not too feasable due to twitter api limitations)
* have a smarter way to handle tweets tooted'
* have a quick service script for systemd? perhaps modify the script a more cron-friendly job


## Reasoning
I've was trying to find a mastodon bot that would allow me to do the above but most of the ones involve reposting *my* tweets to mastodon, or reposting a single account, whatever.
I wanted something that would clone my timeline but all the bots out there simply couldn't since reposting-per-account would reach the twitterapi-calls limit (see below).

With my bot I simply copy as many last tweets have landed on my timeline in a single go (200) and repost them to Mastodon.

This script has a couple timers for that (twitter one explained below) and the Mastodon timer is just one sec between toots. I couldn't find what was the limit on https://botsin.space so I went with that and so far so good.

## Twitter API caveats
TwitterAPI allows you to make 15 calls every 15 mins. In other words, you could make 15 calls in a sec but then you'll need to wait 15 mins until you can make another call.
Due to this I made this bot always wait until 60s are completed such that 15 calls will be me through the 15 mins.